var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'bvcc-app'
        },
        port: process.env.PORT || 8080,
        db: { db: 'bvcc_development' }
    },

    test: {
        root: rootPath,
        app: {
            name: 'bvcc-app'
        },
        port: process.env.PORT || 3000,
        db: { db: 'bvcc_test' }
    },

    production: {
        root: rootPath,
        app: {
            name: 'bvcc-app'
        },
        port: process.env.PORT || 3000,
        db: {
            db: 'bvcc',
            production: 'app',
        }
    }
};

module.exports = config[env];
