var express = require('express'),
    router = express.Router(),
    models = require('../models'),
    Article = models.Article;

module.exports = function(app) {
    app.use('/membership', router);
};
router.get('/', function(req, res, next) {
        res.render('index', {
			layout: 'main',
            title: 'BlackStone Valley Chamber',
        });
});

