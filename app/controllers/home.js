var express = require('express'),
    router = express.Router(),
    models = require('../models'),
    Article = models.Article;

module.exports = function(app) {
    app.use('/', router);
};
router.get('/', function(req, res, next) {
        res.render('index', {
			layout: 'main',
            title: 'BlackStone Valley Chamber',
        });
});




// router.get('/', function(req, res, next) {
//     Article.run().then(function(articles) {
//         res.render('index', {
//             title: 'BlackStone Valley Chamber',
//             "nav-item": false,
// 			layout: 'about',
// 			articles: articles
//         });
//     });
// });

