module.exports = {
    services: [
        { name: 'Marketing and PR' },
        { name: 'Office Space' },
        { name: 'Certificates of Origin' },
        { name: 'Notary Services' },
        { name: 'Sponsorship' },
    ]
}
